import { IsOptional } from 'class-validator';

export class StudentDto {
  @IsOptional()
  uuid: string;
}
