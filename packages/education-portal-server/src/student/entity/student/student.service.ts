import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './student.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class StudentService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: MongoRepository<Student>,
  ) {}

  async find(query?) {
    return await this.studentRepository.find(query);
  }

  async create(student: Student) {
    const studentObject = new Student();
    Object.assign(studentObject, student);
    return await this.studentRepository.insertOne(studentObject);
  }

  async findOne(param, options?) {
    return await this.studentRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.studentRepository.manager.connection
      .getMetadata(Student)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.studentRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.studentRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.studentRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.studentRepository.updateOne(query, options);
  }
}
