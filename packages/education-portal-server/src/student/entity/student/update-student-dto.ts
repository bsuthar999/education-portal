import { IsNotEmpty } from 'class-validator';

export class UpdateStudentDto {
  @IsNotEmpty()
  uuid: string;
}
