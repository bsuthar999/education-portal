import { IEvent } from '@nestjs/cqrs';
import { Student } from '../../entity/student/student.entity';

export class StudentUpdatedEvent implements IEvent {
  constructor(public updatePayload: Student) {}
}
