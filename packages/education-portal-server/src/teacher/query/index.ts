import { RetrieveTeacherQueryHandler } from './get-teacher/retrieve-teacher.handler';
import { RetrieveTeacherListQueryHandler } from './list-teacher/retrieve-teacher-list.handler';

export const TeacherQueryManager = [
  RetrieveTeacherQueryHandler,
  RetrieveTeacherListQueryHandler,
];
