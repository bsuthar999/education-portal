import { IsNotEmpty } from 'class-validator';
export class UpdateTeacherDto {
  @IsNotEmpty()
  uuid: string;
}
