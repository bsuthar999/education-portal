import { InjectRepository } from '@nestjs/typeorm';
import { Teacher } from './teacher.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class TeacherService {
  constructor(
    @InjectRepository(Teacher)
    private readonly teacherRepository: MongoRepository<Teacher>,
  ) {}

  async find(query?) {
    return await this.teacherRepository.find(query);
  }

  async create(teacher: Teacher) {
    const teacherObject = new Teacher();
    Object.assign(teacherObject, teacher);
    return await this.teacherRepository.insertOne(teacherObject);
  }

  async findOne(param, options?) {
    return await this.teacherRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.teacherRepository.manager.connection
      .getMetadata(Teacher)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.teacherRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.teacherRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.teacherRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.teacherRepository.updateOne(query, options);
  }
}
