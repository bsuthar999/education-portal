import { Module, HttpModule } from '@nestjs/common';
import { TeacherAggregatesManager } from './aggregates';
import { TeacherEntitiesModule } from './entity/entity.module';
import { TeacherQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { TeacherCommandManager } from './command';
import { TeacherEventManager } from './event';
import { TeacherController } from './controllers/teacher/teacher.controller';
import { TeacherPoliciesService } from './policies/teacher-policies/teacher-policies.service';

@Module({
  imports: [TeacherEntitiesModule, CqrsModule, HttpModule],
  controllers: [TeacherController],
  providers: [
    ...TeacherAggregatesManager,
    ...TeacherQueryManager,
    ...TeacherEventManager,
    ...TeacherCommandManager,
    TeacherPoliciesService,
  ],
  exports: [TeacherEntitiesModule],
})
export class TeacherModule {}
